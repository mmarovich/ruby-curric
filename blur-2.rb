class Image
  attr_accessor :image

  def initialize(image)
    @image = image
    @ones = []
  end

  def output_image
    for list in @image
      for num in list
        print num
      end
      puts "\n"
    end
  end

  def blur
    getOnes
    @ones.each do |item|
      @image[item[0] - 1] ? @image[item[0] - 1][item[1]] = 1 : nil
      @image[item[0]][item[1] - 1] ? @image[item[0]][item[1] - 1] = 1 : nil
      @image[item[0]][item[1] + 1] ? @image[item[0]][item[1] + 1] = 1 : nil
      @image[item[0] + 1] ? @image[item[0] + 1][item[1]] = 1 : nil
    end
  end

  def getOnes
    @image.each_with_index do |item, imageIndex|
      item.each_with_index do |num, rowIndex|
        if num == 1
          @ones.push([imageIndex, rowIndex])
        end
      end
    end
    puts @ones.inspect
  end
end

image = Image.new([
  [0, 0, 0, 0, 0, 0],
  [0, 1, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 1, 0],
  [0, 0, 0, 0, 0, 0]
])

image.output_image
image.blur(2)
image.output_image