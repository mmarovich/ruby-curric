
# class is used to predefine objects/hashes
class Card
  attr_reader :suit # attr_reader allows the return of input (see line 15, same thing). For encapsulation purposes.
  attr_writer :suit # attr_writer allows you to change an existing value (see line 19, same thing)
  attr_accessor # does attr_reader and attr_writer at the same time.

  # initialize, also known as a constructor, will take in arguments via parameters to use within the class.
  def initialize(rank, suit) # parameters are used within the scope of initialize
    # the @ sign is convention to designate an instance variable, and these will be used within the scope of the class
    @rank = rank
    @suit = suit
  end

  def rank # A function that gets and returns the value of a variable.  A "getter" function
    @rank # returning this variable.
  end

  def rank=(rank) # A function that allows you to change the value of a variable.  A "setter" function.
    @rank = rank
  end

  def output_card # A function in an object is called a method.  This method displays the card in the terminal
    puts "#{@rank} of #{@suit}"
    puts "#{self.rank} of #{self.suit}" # accesses same variables through self, which references the object itself.
  end

  def Card.random_card # class level method - you can call when there is no instance of an object/hash. should use self instead of Card.
    Card.new(rand(1..10), :spades)
  end

end

card = Card.new(10, :spades) # create a new hash using our class.
card2 = Card.random_card # envokes the class level method

puts card.rank # takes advantage of the getter function from line 15
card.rank = 9 # takes advantage of the setter function from line 19
puts card.rank # uses the getter function to output the new value from line 38.

puts card.suit # takes advantage of the attr_reader from line 4.
card.suit = :diamonds # takes advantage of the attr_writer from line 5.
puts card.suit # uses the attr_reader to output the new value from line 42.

puts card.output_card # runs our method defined on line 23
puts card.inspect # shows the object / hash that we made.

puts card2.output_card # will show our randomly generated card from line 35