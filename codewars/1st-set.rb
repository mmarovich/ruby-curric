# Multiply

def multiple(a, b)
  a * b
end

# Odd or Even

def even_or_odd(number)
  remainder = number % 2
  if remainder == 0
    return "Even"
  else
    return "Odd"
  end
end

def even_or_odd(number)
  number.even? ? "Even" : "Odd"
end

def even_or_odd(number)
  (number % 2 == 0) ? "Even" : "Odd"
end

# Double the Integer

def double_integer(i)
  return i + i
end

# Regular Ball / Super Ball

class Ball
  attr_reader :ball_type
  def initialize(ball_type="regular")
    @ball_type = ball_type
  end
end

class Ball
  attr_accessor :ball_type
  def initialize(type = 'regular')
    self.ball_type = type
  end
end

# Broken Sequence

def find_missing_number(sequence)
  number_sequence = sequence.split.map(&:to_i).sort
  print sequence.split
  
  # number_sequence.each.with_index(1) do |actual, expected|
  #   return expected unless actual == expected
  # end
  
  0
end

find_missing_number("4 2 3 1")
