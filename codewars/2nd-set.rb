# Remove Duplicates

def unique(integers)
  return integers.uniq
end

# Color Ghost

class Ghost
  def color
    ["white","yellow","purple","red"].sample
  end
end

class Ghost
  def color
    %w(white yellow purple red).sample
  end
end

class Ghost
  COLORS = %w(white yellow purple red)
  attr_accessor :color
  
  def initialize
    @color = COLORS.sample
  end
end

# Arithmatic Sequence
# Here, `first` is the number we start at, `n` is the index with the value we want, and `c` is the incrementation

# The original formula for this is `first - (c - 1) * n`, but that first minus is conditional.  You can change it to a plus
# sign and if the sequence is deincrementing, you can add the minus to that number through argument. The second minus only
# applies if you are starting at index 1, which in coding, you start at index 0 in the first place.
# khan academy video: https://www.khanacademy.org/math/algebra/sequences/constructing-arithmetic-sequences/v/finding-the-100th-term-in-a-sequence

def nthterm(first, n, c)
  first + n * c
end

# Sentence Smash!

def smash(words)
  words.join(" ")
end

# 2 + 2 * 2

def calculate
  (2 + 2) * 2
end