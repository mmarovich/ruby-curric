##### Grader

def grader(score)
  if score > 1
    return "F"
  elsif score >= 0.9
    return "A"
  elsif score >= 0.8
    return "B"
  elsif score >= 0.7
    return "C"
  elsif score >= 0.6
    return "D"
  else
    return "F"
  end
end

def grader(score)
  return 'F' if score > 1 || score < 0.6
  return 'A' if score >= 0.9
  return 'B' if score >= 0.8
  return 'C' if score >= 0.7
  return 'D' if score >= 0.6
end

def grader(score)
  case score
    when 0.6...0.7 then "D"
    when 0.7...0.8 then "C"
    when 0.8...0.9 then "B"
    when 0.9..1 then "A"
    else "F"
  end
end

#####

##### Sum Array
# explaination of .inject: https://stackoverflow.com/questions/710501/need-a-simple-explanation-of-the-inject-method

def sum(numbers)
  total = 0
  numbers.each { |n| total += n}
  total
end

# Structure of inject: () <-- what index you start at. block is respective variables: cumulative, and value of index.
# Lastly, end of block is what you want to execute over each iteration.
# inject is the same as reduce method.

def sum(numbers)
  numbers.inject(0) { |total, n| total + n}
end

# shortcut = :+ symbol.

def sum(numbers)
  numbers.inject(:+) # or (0,:+) otherwise first value defaults to nil
end

#####

##### Convert boolean values to strings 'Yes' or 'No'

def bool_to_word bool
  if bool == true
    return 'Yes'
  elsif bool == false
    return 'No'
  end
end

def bool_to_word bool
  return "No" if bool == false
  return "Yes" if bool == true
end

def bool_to_word bool
  bool ? 'Yes' : 'No'
end

#####

##### Convert a Number to a String

def numberToString(num)
  num.to_s
end

def numberToString(num)
  "#{num}"
end
