##### Welcome to the City

def say_hello(name, city, state)
  "Hello, #{name.join(' ')}! Welcome to #{city}, #{state}!"
end

def say_hello(name, city, state)
  "Hello, %s! Welcome to %s, %s!" % [name.join(" "), city, state]
end

#####

##### Basic Variable Assignment

a = "code"
b = "wa.rs"
name = a + b

#####

##### Get Planet Name By ID

def get_planet_name(id)
  name = ''
  case id
    when 1 
      name = "Mercury"
    when 2 
      name = "Venus"
    when 3 
      name = "Earth"
    when 4 
      name = "Mars"
    when 5 
      name = "Jupiter"
    when 6 
      name = "Saturn"
    when 7 
      name = "Uranus"  
    when 8 
      name = "Neptune"
  end
  return name
end

def get_planet_name(id)
  case id
    when 1 then "Mercury"
    when 2 then "Venus"
    when 3 then "Earth"
    when 4 then "Mars"
    when 5 then"Jupiter"
    when 6 then "Saturn"
    when 7 then "Uranus"  
    when 8 then "Neptune"
  end
  
end

def get_planet_name(id)
  ['Mercury','Venus','Earth','Mars','Jupiter','Saturn','Uranus','Neptune'][id-1]
end

def get_planet_name(id)
  %w[0 Mercury Venus Earth Mars Jupiter Saturn Uranus Neptune][id]
end

def get_planet_name(id)
  %w{Mercury Venus Earth Mars Jupiter Saturn Uranus Neptune}[id - 1]
end

#####

##### Basic Training: Add item to an Array

websites.push("codewars")

websites << 'codewars'

websites.insert(websites.length,"codewars")

#####

##### Short Long Short

def solution(a, b)
  if a.length < b.length
    return a + b + a
  else
    return b + a + b
  end
end

def solution(a, b)
  a.length < b. length ? a + b + a : b + a + b 
end