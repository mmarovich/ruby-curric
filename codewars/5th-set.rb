##### Function 1 - hello world

def greet
  'hello world!'
end

def greet; 'hello world!'; end

#####

##### Function 2 - squaring an argument

def square(n)
  n ** 2
end

def square(i)
  i * i
end

#####

##### Function 3 - multiplying two numbers

def multiply(a,b)
  a * b
end

def multiply x, y
  x * y
end

#####

##### Push a hash/an object into array

items = Array.new
items << {:a => "b", :c => "d"}

items = Array.new
items.push({:a => "b", :c => "d"})

#####

##### Refactored Greeting

class Person
  attr_reader :name
  def initialize(name)
    @name = name
  end

  def greet(yourName)
    "Hello #{yourName}, my name is #{@name}"
  end
end

#####

##### Statistics Algorithm - Calculate Mean

def calc_mean(ary)
  if !ary.is_a?(Array)
    0
  elsif ary.empty?
    0
  else
    ary.inject(0,:+) / ary.length
  end
end

def calc_mean(ary)
  ary.reduce(:+)/ary.size rescue 0
end