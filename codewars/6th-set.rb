##### Ruby Person Class Bug

class Person
  def initialize(firstName, lastName, age)
    @firstName = firstName
    @lastName = lastName
    @age = age
  end
  
  def full_name
    "#{@firstName} #{@lastName}"
  end
  
  def age
    @age
  end
end

class Person
  attr_reader :age
  
  def initialize(firstName, lastName, age)
    @firstName = firstName
    @lastName = lastName
    @age = age
  end
  
  def full_name
    "#{@firstName} #{@lastName}"
  end
end

#####

##### No oddities here

def no_odds( values )
  evens = []
  for int in values
    if int % 2 == 0
      evens.push(int)
    end
  end
  return evens
end

# & creates a proc (see third example) followed by condition

def no_odds( values )
  values.select &:even?
end

def no_odds( values )
  values.reject(&:odd?)
end

def no_odds( values )
  values.select{|a| a.even? }
end

#####

##### Greet Me

def greet(name)
  cappedName = name.capitalize
  return "Hello #{cappedName}!"
end

def greet(name)
  "Hello #{name.capitalize}!"
end

def greet(name)
  "Hello %s!" % name.capitalize
end

#####

##### Digitize

def digitize(n)
  n.to_s.split('').map(&:to_i)
end

def digitize(n)
  n.to_s.chars.map(&:to_i)
end

# digits method "extracts digit after digit". 

def digitize(n)
  n.digits.reverse
end

#####

##### Square(n) Sum

def squareSum(numbers)
  numSqrs = numbers.map! {|n| n * n}
  return numSqrs.inject(:+)
end

def squareSum(numbers)
  numbers.map {|n| n*n}.reduce(:+)
end

def squareSum(numbers)
  numbers.reduce { |s, n| s+=n**2 }
end

#####

##### Reversed Words

def solution(sentence)
  sentence.split(' ').reverse.join(" ")
end

def solution(sentence)
  sentence.split.reverse.join(" ")
end