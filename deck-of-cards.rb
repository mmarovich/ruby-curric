class Card
  attr_accessor :rank, :suit

  def initialize(rank, suit)
    @rank = rank
    @suit = suit
  end

  def output_card
    puts "#{self.rank} of #{self.suit}"
  end

  def self.random_card
    Card.new(rand(10), :spades)
  end
end

class Deck
  def initialize
    @cards = []
    @hand1 = []
    @hand2 = []
    1.upto(13) do |n|
      @cards << Card.new(n, :spades)
      @cards << Card.new(n, :diamonds)
      @cards << Card.new(n, :hearts)
      @cards << Card.new(n, :clubs)
    end
  end

  def shuffle
    @cards.shuffle!
  end

  def output
    @cards.each do |card|
      card.output_card
    end
  end

  def deal
    @hand1 << @cards.shift
    @hand2 << @cards.shift
  end

  def showhand(handNum)
    if handNum == 1
      @hand1.each do |card|
        card.output_card
      end
    else
      @hand2.each do |card|
        card.output_card
      end
    end
  end
end

deck = Deck.new
deck.shuffle

deck.deal
deck.showhand(1)
deck.showhand(2)

deck.deal
deck.showhand(1)
deck.showhand(2)