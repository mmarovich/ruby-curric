
def foobar
  1.upto(100) do |n|
    if n % 15 == 0
      puts "foobar" 
    elsif n % 5 == 0
      puts "bar"      
    elsif n % 3 == 0
      puts "foo"     
    else
      puts n
    end
  end
end

foobar()