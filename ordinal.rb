def what_is_ordinal(num)
  last_num = num % 10
  last_2_nums = num % 100

  if last_2_nums == 11 || last_2_nums == 12 || last_2_nums == 13 || last_num == 4 || last_num == 5 || last_num == 6 || 
    last_num == 7 || last_num == 8 || last_num == 9 || last_num == 0
    return "th"
  elsif last_num == 1
    return "st"
  elsif last_num == 2
    return "nd"
  elsif last_num == 3
    return "rd"
  end
end

ordinal = what_is_ordinal(num)
puts "That is the #{num}#{ordinal} item."