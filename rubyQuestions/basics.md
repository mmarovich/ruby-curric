## Comments and Pound Characters

### Why does the `#` in `puts "Hi # there."` not comment?

The `#` in that code is inside a string, so it will be put into the string until the ending `"` character is hit. Pound characters in string are just considered characters, not comments.

### How do I comment out multiple lines?

Put a `#` in front of each one.

### Why is the `%` character a "modulus" and not a "percent"?


Mostly that's just how the designers chose to use that symbol. In normal writing you are correct to read it as a "percent." In programming this calculation is typically done with simple division and the `/` operator. The `%` modulus is a different operation that just happens to use the `%` symbol.

### How does `%` work?

Another way to say it is, "X divided by Y with J remaining." For example, "100 divided by 16 with 4 remaining." The result of `%` is the J part, or the remaining part.

### What is the order of operations?

Parentheses Exponents (Multiplication and Division) (Addition and Subtraction)

### What is the difference between `=` (single-equal) and `==` (double-equal)?

The `=` (single-equal) assigns the value on the right to a variable on the left. The `==` (double-equal) tests whether two things have the same value.

### Can we write `x=100` instead of `x = 100`?

You can, but it's bad form. You should add space around operators like this so that it's easier to read.

### What is the difference between a float and an integer?

An integer is a number without a decimal.  A float is a number with a decimal.

### Can I make a variable like this: `1 = 'Zed Shaw'`?

No, `1` is not a valid variable name. They need to start with a character, so `a1` would work, but `1` will not.

### What is the difference between a variable and a string?

A variable represents data and does not have quotes around it.  A string is a group of characters surrounded by quotes.

### What is the purpose of comments?  Do you need to comment every line?

The purpose of comments is to explain to other people what is going on.  It is curteous to your team or anyone else who is trying to read your code.  You don't need to comment every line, but it is important to explain things moderately.

### Can I use single-quotes or double-quotes to make a string or do they do different things?

In Ruby the `"` (double-quote) tells Ruby to replace variables it finds with `#{}`, but the `'` (single-quote) tells Ruby to leave the string alone and ignore any variables inside it.

### Should I use `%{}` or `#{}` for formatting?

You will almost always use `#{}` to format your strings, but there are times when you want to apply the same format to multiple values. That's when `%{}` comes in handy.

### Why do I have to put quotes around "one" but not around `true` or `false`?

Ruby recognizes true and false as boolean values, a separate datatype from strings. If you put quotes around them then they are turned into strings and won't work.

### What does `\` do in ruby?

It's an escape character.

### what is the method for converting a string to an integer? Integer to a string? String to decimal number? Prompting the user and trimming the string?

.to_i
.to_s
.to_f
gets.chomp

### What's the difference between `ARGV` and `gets.chomp`?

The difference has to do with where the user is required to give input. If they give your script inputs on the command line, then you use `ARGV`. If you want them to input using the keyboard while the script is running, then use `gets.chomp`.

### What is the datatype of the command line arguments?

String