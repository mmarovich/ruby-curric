## Which of the expressions listed below will result in "false"?

```
true    ? "true" : "false"
false   ? "true" : "false"
nil     ? "true" : "false"
1       ? "true" : "false"
0       ? "true" : "false"
"false" ? "true" : "false"
""      ? "true" : "false"
[]      ? "true" : "false"
```























## Answer


In Ruby, the only values that evaluate to false are `false` and `nil`. Everything else – even zero (0) and an empty array ([]) – evaluates to true.

This comes as a real surprise to programmers who have previously been working in other languages like JavaScript.
